TARGET_EXEC ?= GameEngine.out

BUILD_DIR ?= DebugGCC
SRC_DIRS ?= GameEngine

SRCS := $(shell find $(SRC_DIRS) -name *.cpp -or -name *.c -or -name *.s)
OBJS := $(SRCS:%=$(BUILD_DIR)/%.o)
DEPS := $(OBJS:.o=.d)

MKDIR_P := mkdir -p

INC_DIRS := $(shell find $(SRC_DIRS) -type d)
INC_FLAGS := $(addprefix -I,$(INC_DIRS))

VCPKG_HOME := /home/cieric/source/repos/vcpkg

CPPFLAGS ?= -g -O3 -I$(VCPKG_HOME)/installed/x64-linux/include $(INC_FLAGS) -MMD -MP -ggdb -DGLEW_STATIC -DGLEW_NO_GLU -DFMT_HEADER_ONLY -std=c++2a

LIB_NAMES := glfw GL GLEW Xxf86vm Xrandr Xext X11 dl
LIBS := $(LIB_NAMES:lib%=%)
LIBS_FLAGS := $(addprefix -l,$(LIBS))
LDFLAGS ?= -m64 -L$(VCPKG_HOME)/installed/x64-linux/lib -L/usr/lib/x86_64-linux-gnu $(LIBS_FLAGS) -pthread

$(BUILD_DIR)/$(TARGET_EXEC): $(OBJS)
	$(CXX) $(OBJS) $(LDFLAGS) -o $@

# assembly
$(BUILD_DIR)/%.s.o: %.s
	$(MKDIR_P) $(dir $@)
	$(AS) $(ASFLAGS) -c $< -o $@

# c source
$(BUILD_DIR)/%.c.o: %.c
	$(MKDIR_P) $(dir $@)
	$(CC) $(CPPFLAGS) $(CFLAGS) -c $< -o $@

# c++ source
$(BUILD_DIR)/%.cpp.o: %.cpp
	$(MKDIR_P) $(dir $@)
	$(CXX) $(CPPFLAGS) $(CXXFLAGS) -c $< -o $@

.PHONY: clean

clean:
	$(RM) -r $(BUILD_DIR)

run:
	cd GameExample && ../$(BUILD_DIR)/$(TARGET_EXEC)

-include $(DEPS)