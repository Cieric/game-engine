#pragma once

#include "GameEngine/Utils.h"
#include "GameEngine/Math.h"
#include "GameEngine/Predeclare.h"
#include "GameEngine/Objects/ObjectBase.h"
#include "GameEngine/Objects/Components/ComponentBase.h"
#include "GameEngine/Assets/Model.h"

struct Engine
{
	bool resized = false;
};