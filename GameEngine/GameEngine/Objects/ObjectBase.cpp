#include "ObjectBase.h"

ObjectBase::ObjectBase()
{
	for (std::size_t i = _globalComponentIds.size(); i > 0; i--)
		componentIds.push_back(std::vector<Id_T>());
}

Mat4f& ObjectBase::getTransform()
{
	return transform;
}
