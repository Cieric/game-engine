#include "ComponentGlobalIDManager.h"

std::unordered_map<size_t, size_t> _globalComponentIds = {};
std::unordered_map<size_t, ConstructComponent> _globalComponentConstructors = {};