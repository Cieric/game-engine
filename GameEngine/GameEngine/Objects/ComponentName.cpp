#include "ComponentName.h"

size_t hash_c_string(const char* str)
{
    size_t result = 0;
    const size_t prime = 31;
    for (const char* c = str; *c != 0; c++)
        result = *c + (result * prime);
    return result;
}