#include "Utils.h"
#include <fstream>

namespace fs = std::filesystem;

void Utils::PrintError(std::string str)
{
    fmt::print(fg(fmt::color::crimson), "[{:%H:%M:%S}][ERROR] {}", std::chrono::system_clock::now(), str);
}

const char* Utils::ReadFile(std::filesystem::path path)
{
    if (!fs::exists(path)) {
        PrintError("Failed to read file \"{}\"\n", path.generic_string());
        return {};
    }

    std::ifstream is(path.generic_string(), std::ios::binary);
    is.seekg(0, std::ios_base::end);
    std::size_t size = (std::size_t)is.tellg();
    is.seekg(0, std::ios_base::beg);

    char* data = (char*)::operator new(size + 1);
    
    if (!data) {
        PrintError("Failed to malloc \"{}\"\n", path.generic_string());
        return {};
    }

    data[size] = 0;
    is.read(data, size);
    is.close();

    return data;
}