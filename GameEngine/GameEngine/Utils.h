#pragma once
#include <string.h>
#include <filesystem>
#include <iostream>
#include <fmt/core.h>
#include <fmt/color.h>
#include <fmt/chrono.h>
#include <ctime>
#include <optional>
#include "Utils/handle_map.h"
#include "Utils/GEMalloc.h"

#define CAT2(a,b) a##b
#define CAT(a,b) CAT2(a,b)
#define UNIQUE_ID CAT(CAT(_uid,__COUNTER__),__LINE__)
#define Initialize__impl(tempname, func) static const int UNIQUE_ID = []func();
#define Initialize(func) Initialize__impl(UNIQUE_ID, func)
#define ToStr(x) #x

#define DEFER(fn) Utils::ScopeGuard CAT(__defer__, __LINE__) = [&] ( ) { fn ; }

namespace Utils
{
    void PrintError(std::string str);
    template<typename ...Args>
    void PrintError(std::string str, Args... args)
    {
        PrintError(fmt::format(str, args...));
    }

    const char* ReadFile(std::filesystem::path path);

    class ScopeGuard {
    public:
        template<class Callable>
        ScopeGuard(Callable&& fn) : fn_(std::forward<Callable>(fn)) {}

        ScopeGuard(ScopeGuard&& other) noexcept : fn_(std::move(other.fn_)) {
            other.fn_ = nullptr;
        }

        ~ScopeGuard() {
            if (fn_) fn_();
        }

        ScopeGuard(const ScopeGuard&) = delete;
        void operator=(const ScopeGuard&) = delete;

    private:
        std::function<void()> fn_;
    };
}