#pragma once
#include "../Utils.h"
#include "../Math.h"

struct Material;

class IShader
{
public:
	virtual void LoadVertexShader(std::filesystem::path path) = 0;
	virtual void LoadFragmentShader(std::filesystem::path path) = 0;
	virtual void Build() = 0;
	virtual void applyMaterial(Material& mat) = 0;
	virtual Id_T FindBindPoint(std::string name) = 0;
	virtual void Use() = 0;

	virtual bool Bind(Id_T handle, const Mat4f value) = 0;
	virtual bool Bind(Id_T handle, const float value) = 0;
	virtual bool Bind(Id_T handle, const double value) = 0;
	virtual bool Bind(Id_T handle, const int value) = 0;
	virtual bool Bind(Id_T handle, const Vec3f value) = 0;
};
