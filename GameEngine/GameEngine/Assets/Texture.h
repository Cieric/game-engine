#pragma once
#include <string>
#include <filesystem>
#include <unordered_map>

struct Texture {
    unsigned int id;
    std::string type;
    std::filesystem::path path;
};

unsigned int TextureFromFile(const char* path, const std::string& directory, bool gamma);