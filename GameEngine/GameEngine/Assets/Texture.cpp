#include "Texture.h"
#include "../Utils.h"
#include <GL/glew.h>
//#define STB_IMAGE_IMPLEMENTATION
//#include <stb_image.h>

std::unordered_map<std::string, unsigned int> loadedTextures;

unsigned int TextureFromFile(const char* path, const std::string& directory, bool gamma)
{
    std::string filename = std::string(path);
    if (filename == "D:\\EVERYDAY 4\\Wolf realistic\\Wolf\\wolf_a_body_alpha_col.tga")
        filename = "wolf_a_body_alpha_col.tga";
    filename = directory + '/' + filename;

    if (loadedTextures.find(filename) != loadedTextures.end())
        return loadedTextures[filename];

    unsigned int textureID;
    glGenTextures(1, &textureID);

    int width=0, height=0, nrComponents=0;
    unsigned char* data = nullptr;// stbi_load(filename.c_str(), &width, &height, &nrComponents, 0);
    if (data)
    {
        GLenum format;
        if (nrComponents == 1)
            format = GL_RED;
        else if (nrComponents == 3)
            format = GL_RGB;
        else if (nrComponents == 4)
            format = GL_RGBA;

        glBindTexture(GL_TEXTURE_2D, textureID);
        glTexImage2D(GL_TEXTURE_2D, 0, format, width, height, 0, format, GL_UNSIGNED_BYTE, data);
        glGenerateMipmap(GL_TEXTURE_2D);

        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

        //stbi_image_free(data);
    }
    else
    {
        Utils::PrintError("Texture failed to load at path: {}\n", path);
        //stbi_image_free(data);
    }

    return loadedTextures[filename] = textureID;
}