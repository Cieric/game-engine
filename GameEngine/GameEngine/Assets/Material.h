#pragma once
#include "Texture.h"
#include "../Math.h"

struct Material
{
	std::vector<Texture> Textures;
	Texture TexDiffuse;
	Texture TexSpecular;
	Vec4f Color;
	float Transparency;
};