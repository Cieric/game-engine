#include "GEMalloc.h"
#include <unordered_set>
#include <stdint.h>
#include <fmt/format.h>
#include <fmt/color.h>
#include "HashMap.h"

struct Allocation
{
	void* address;
	size_t bytes;
	void* returnAddr;
};

class MemoryUsageStats
{
public:
	size_t allocated;
	size_t freed;
	HashMap<uintptr_t, Allocation> allocations;
};

bool disable = true;
std::unordered_set<void*> breakpoints;
MemoryUsageStats memoryUsageStats;

void MemoryUsageInitialize()
{
//	disable = false;
}

void PrintMemoryUsage()
{
//	fmt::print(fg(fmt::color::yellow), "Allocated {} bytes!\n", memoryUsageStats.allocated - memoryUsageStats.freed);
//	std::unordered_map<void*, size_t> bytesFromLocs = {};
//	for (auto row : memoryUsageStats.allocations.table)
//		for (auto entry : row)
//			bytesFromLocs[entry.getValue().returnAddr] += entry.getValue().bytes;
//
//	for (auto locs : bytesFromLocs)
//		fmt::print(fg(fmt::color::yellow), "+--[{}] {}\n", locs.first, locs.second);
}

void SetBreakpointOnLeaks()
{
//	disable = true;
//	breakpoints.clear();
//	std::unordered_map<void*, size_t> bytesFromLocs = {};
//	for (auto row : memoryUsageStats.allocations.table)
//		for (auto entry : row)
//			bytesFromLocs[entry.getValue().returnAddr] += entry.getValue().bytes;
//
//	for (auto locs : bytesFromLocs)
//		breakpoints.insert(locs.first);
//	disable = false;
}

void ResetMemoryUsageStats()
{
//	memoryUsageStats.allocated = 0;
//	memoryUsageStats.freed = 0;
//	memoryUsageStats.allocations.clear();
}

//void* operator new(size_t size)
//{
//	void* calledFrom = _ReturnAddress();
//	auto ret = malloc(size);
//	memoryUsageStats.allocated += size;
//	if (!disable)
//	{
//		disable = true;
//		if (breakpoints.find(calledFrom) != breakpoints.end())
//			__debugbreak();
//		memoryUsageStats.allocations.put((uintptr_t)ret, Allocation{
//			ret,
//			size,
//			calledFrom
//		});
//		disable = false;
//	}
//	return ret;
//}
//
//void operator delete(void* memory, size_t size)
//{
//	memoryUsageStats.freed -= size;
//	if (!disable)
//	{
//		disable = true;
//		memoryUsageStats.allocations.remove((uintptr_t)memory);
//		disable = false;
//	}
//	return free(memory);
//}
