#pragma once
#include <array>
#include <vector>
#define TABLE_SIZE 13

template <typename K>
struct KeyHash {
    unsigned long operator()(const K& key) const
    {
        return (uint64_t)(key) % TABLE_SIZE;
    }
};

// Hash node class template
template <typename K, typename V>
class HashNode {
public:
    HashNode(const K& key, const V& value) :
        key(key), value(value), next(NULL) {
    }

    K getKey() const {
        return key;
    }

    V getValue() const {
        return value;
    }

    void setValue(V value) {
        HashNode::value = value;
    }

    HashNode* getNext() const {
        return next;
    }

    void setNext(HashNode* next) {
        HashNode::next = next;
    }

private:
    // key-value pair
    K key;
    V value;
    // next bucket with the same key
    HashNode* next;
};

template <typename K, typename V, typename F = KeyHash<K>>
class HashMap {
public:
    HashMap() {
    }

    ~HashMap() {}

    bool clear()
    {
        for (auto row : table)
            row.clear();
        return true;
    }

    bool get(const K& key, V& value) {
        unsigned long hashValue = hashFunc(key);
        
        auto row = table[hashValue];

        for (auto& entry : row)
        {
            if (entry->getKey() == key)
            {
                value = entry->getValue();
                return true;
            }
        }
        return false;
    }

    void put(const K& key, const V& value) {
        unsigned long hashValue = hashFunc(key);
        auto row = table[hashValue];
        
        for (auto& entry : row)
        {
            if (entry.getKey() == key)
            {
                entry.setValue(value);
                return;
            }
        }

        row.push_back(HashNode<K, V>(key, value));
    }

    void remove(const K& key) {
        unsigned long hashValue = hashFunc(key);
        auto& row = table[hashValue];
        auto entry = row.begin();
        for (; entry != row.end(); entry++)
            if (entry->getKey() == key)
                break;
        if (entry != row.end())
            row.erase(entry);
    }

    // hash table
    std::array<std::vector<HashNode<K, V>>, TABLE_SIZE> table;
private:
    F hashFunc;
};