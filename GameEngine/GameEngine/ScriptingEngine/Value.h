#pragma once
#include <variant>
#include <string>
#include <string.h>
#include <vector>
#include <assert.h>
#include "../Math.h"

enum ValueType : uint8_t
{
	//POD
	TYPE_INT = 1,
	TYPE_FLOAT,
	TYPE_BOOL,

	//Vectors
	TYPE_INT_2,
	TYPE_FLOAT_2,
	TYPE_BOOL_2,
	TYPE_INT_3,
	TYPE_FLOAT_3,
	TYPE_BOOL_3,
	TYPE_INT_4,
	TYPE_FLOAT_4,
	TYPE_BOOL_4,

	//Matrix
	TYPE_FLOAT_4_4,

	//Complex Types
	TYPE_STRING,
	TYPE_FUNCTION,
	TYPE_ERROR
};

#ifdef _MSC_VER
#define strdup(s) _strdup(s)
#endif

struct Error_t
{
	const char* msg;
	Error_t(std::string msg) : msg(strdup(msg.c_str())) {};
	Error_t(const char* msg) : msg(strdup(msg)) {};
	Error_t(const Error_t& err) : msg(strdup(err.msg)) {};
	~Error_t() { free((void *)msg); }
};

class Interpreter;
struct StmtFunction;
struct Value;
struct Callable
{
	int arity;
	StmtFunction* func;
	Value(*call)(Interpreter* interpreter, Callable* func, std::vector<Value> arguments);
};

struct Value
{
	ValueType type;
	char data[128];

public:
	Value(const Value& copy) : type(copy.type)
	{
		switch (copy.type)
		{
		case TYPE_STRING: new(data) std::string(*(std::string*)copy.data); break;
		default: memcpy(data, copy.data, 128); break;
		}
	}

	Value(ValueType type, std::string value) : type(type) {
		new(data) std::string(value);
	}

	~Value() {
		switch (type) {
		case TYPE_STRING: std::destroy_at((std::string*)data); break;
		default: break;
		}
	}

	template<typename T>
	Value(ValueType type, T value) : type(type) {
		assert(sizeof(T) <= 128);
		memcpy(data, &value, sizeof(T));
	}

	template<typename T>
	T getData() {
		return *(T*)data;
	}
};

std::string to_string(Value value);
std::string to_string(const ValueType value);

//template<typename T>
//static T getcast(Value val)
//{
//	if (val.type == TYPE_ERROR) throw "Error cant getCast an Error value!";
//	switch (val.type)
//	{
//	case TYPE_INT: return (T)val.getData<int64_t>();
//	case TYPE_FLOAT: return (T)val.getData<double>();
//	case TYPE_BOOL: return (T)val.getData<bool>();
//	}
//
//	throw "(getCast) Failed to find Value type!";
//}

template<typename T>
static Value typecast(ValueType type, Value right)
{
	if (right.type == TYPE_ERROR) return right;
	switch (right.type)
	{
	case TYPE_INT: return Value(type, (T)right.getData<int64_t>());
	case TYPE_FLOAT: return Value(type, (T)right.getData<double>());
	case TYPE_BOOL: return Value(type, (T)right.getData<bool>());
	}

	return Value(TYPE_ERROR, Error_t( "unknown typecast" ));
}

static Value matchType(ValueType type, Value val)
{
	if (val.type == TYPE_ERROR) return val;
	if (type == val.type) return val;

	switch (type)
	{
	case TYPE_INT: return typecast<int64_t>(type, val);
	case TYPE_FLOAT: return typecast<double>(type, val);
	case TYPE_BOOL: return typecast<bool>(type, val);
	case TYPE_STRING: {
		if (val.type == TYPE_STRING)
			return val;
	} break;
	case TYPE_FLOAT_2: {
		switch (val.type)
		{
		case TYPE_FLOAT: return Value(TYPE_FLOAT_2, Vec2d{ val.getData<double>(), val.getData<double>() });
		}
	}
	case TYPE_FLOAT_3: {
		switch (val.type)
		{
		case TYPE_FLOAT: return Value(TYPE_FLOAT_3, Vec3d{ val.getData<double>(), val.getData<double>(), val.getData<double>() });
		}
	}
	}

	return Value(TYPE_ERROR, Error_t( "unknown type used in matchType" ));
}