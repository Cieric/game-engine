#pragma once
#include <unordered_map>
#include <string>

#include "Value.h"

class Environment {
	std::unordered_map<std::string, Value> values = {};
public:
	Environment* enclosing;

	Environment();
	Environment(Environment* enclosing);

	void define(std::string name, ValueType type, Value value);
	void defineFunction(std::string name, Callable value);
	Value get(std::string name);
	Value getFunction(std::string name, size_t arity);
	Value assign(std::string name, Value value);
};