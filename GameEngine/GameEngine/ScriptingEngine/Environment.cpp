#include "Environment.h"
#include <string>
#include <fmt/format.h>

Environment::Environment() :
	enclosing(nullptr)
{}

Environment::Environment(Environment* enclosing) :
	enclosing(enclosing)
{}

void Environment::define(std::string name, ValueType type, Value value)
{
	if (value.type != type)
		value = matchType(type, value);
	values.insert_or_assign(name, value);
}

void Environment::defineFunction(std::string name, Callable value)
{
	std::string fullname = fmt::format("{}_{}", name, value.arity);
	values.insert_or_assign(fullname, Value(TYPE_FUNCTION, value));
}

Value Environment::getFunction(std::string name, size_t arity)
{
	std::string fullname = fmt::format("{}_{}", name, arity);
	auto key = values.find(fullname);
	if (key != values.end()) {
		return key->second;
	}

	if (enclosing != nullptr) return enclosing->get(fullname);

	return Value(TYPE_ERROR, Error_t(std::string("Undefined variable '" + name + "'.").c_str()));
}

Value Environment::get(std::string name)
{
	auto key = values.find(name);
	if (key != values.end()) {
		return key->second;
	}

	if (enclosing != nullptr) return enclosing->get(name);

	return Value(TYPE_ERROR, Error_t(std::string("Undefined variable '" + name + "'.").c_str()));
}

Value Environment::assign(std::string name, Value value) {
	auto found = values.find(name);
	if (found != values.end()) {
		value = matchType(found->second.type, value);
		values.insert_or_assign(name, value);
		return value;
	}

	if (enclosing != nullptr) {
		return enclosing->assign(name, value);
	}

	return Value(TYPE_ERROR, Error_t( "Undefined variable '" + name + "'." ));
}