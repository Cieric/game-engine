#version 430
out vec4 FragColor;

struct Material
{
	sampler2D tex_AMBIENT;
	sampler2D tex_AMBIENT_OCCLUSION;
	sampler2D tex_BASE_COLOR;
	sampler2D tex_DIFFUSE;
	sampler2D tex_DIFFUSE2;
	sampler2D tex_DIFFUSE_ROUGHNESS;
	sampler2D tex_DISPLACEMENT;
	sampler2D tex_EMISSION_COLOR;
	sampler2D tex_EMISSIVE;
	sampler2D tex_HEIGHT;
	sampler2D tex_LIGHTMAP;
	sampler2D tex_METALNESS;
	sampler2D tex_NONE;
	sampler2D tex_NORMALS;
	sampler2D tex_NORMAL_CAMERA;
	sampler2D tex_OPACITY;
	sampler2D tex_REFLECTION;
	sampler2D tex_SHININESS;
	sampler2D tex_SPECULAR;
	sampler2D tex_UNKNOWN;
};

uniform Material material;

in vec3 f_position;
in vec3 f_normal;
in vec2 f_texCoord;

in vec3 LightPos;

uniform vec3 lightColor;

void main()
{
    // ambient
    float ambientStrength = 0.1;
    vec3 ambient = ambientStrength * lightColor;

     // diffuse 
    vec3 norm = normalize(f_normal);
    vec3 lightDir = normalize(LightPos - f_position);
    float diff = max(dot(norm, lightDir), 0.0);
    vec3 diffuse = diff * lightColor;
    
    // specular
    float specularStrength = 0.5;
    vec3 viewDir = normalize(-f_position); // the viewer is always at (0,0,0) in view-space, so viewDir is (0,0,0) - Position => -Position
    vec3 reflectDir = reflect(-lightDir, norm);  
    float spec = pow(max(dot(viewDir, reflectDir), 0.0), 32);
    vec3 specular = specularStrength * spec * lightColor; 
    
    vec3 result = (ambient + diffuse + specular) * texture2D(material.tex_DIFFUSE, f_texCoord).rgb;
    FragColor = vec4(result, 1.0);
}